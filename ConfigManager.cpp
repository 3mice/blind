#include "ConfigManager.h"

#include <LittleFS.h>

// create gloab object
//ConfigManager Config;

DeviceConfig *DeviceConfig::init(long version, String filename) {
  _filename = filename;
  deviceId = this->getDeviceId();
  firmwareVersion = version;

  // replace id in any variables that use it
  deviceName.replace("{ID}", deviceId);
  wifi.ap_ssid.replace("{ID}", deviceId);
  
//  this->loadConfig();
}

/**
 * Builder functions
 */

/**
 * Reads the current position
 */
long DeviceConfig::loadPosition(long defaultPosition) {
  this->serialDebug(2, "Loading ccurrent position from file\n");
  long _position = defaultPosition;
  
  if (!LittleFS.begin()) {
    this->serialDebug(1, "Failed to mount file system\n");
    _fsInitialised = false;
  } else {
    this->serialDebug(2, "File system mounted\n");
    _fsInitialised = true;
  }

  if (_fsInitialised) {
    File positionFile = LittleFS.open(_positionFilename, "r");
    if (!positionFile) {
      this->serialDebug(1, "Failed to open position file ");
      this->serialDebug(1, _positionFilename.c_str());
      this->serialDebug(1, "\n");
    } else {
      serialDebug(4, "Config file size: ");
      serialDebug(4, String(positionFile.size()));
      serialDebug(4, " bytes\n");
      const size_t fileSize = positionFile.size();
      if (fileSize > CONFIG_MAX_FILE_SIZE) {
        this->serialDebug(1, "Config file size is too large\n");
      } else if (fileSize == 0) {
        serialDebug(2, "No position file found");
      } else {
        this->serialDebug(5, "Before :- ");
        this->serialDebug(5, String(_position));
        this->serialDebug(5, "\n");
        positionFile.read ((byte*)&_position, sizeof(_position));
        this->serialDebug(5, "After :- ");
        this->serialDebug(5, String(_position));
        this->serialDebug(5, "\n");
        
        // close the file
        positionFile.close();
      }
    }
  }

  return _position;
}

/**
 * Reads a config file from the default file store. The values read 
 * will be appended to the current configuration items;
 */
DeviceConfig *DeviceConfig::loadConfig() {
  this->serialDebug(2, "Loading configuration from file\n");
  
  if (!LittleFS.begin()) {
    this->serialDebug(1, "Failed to mount file system\n");
    _fsInitialised = false;
  } else {
    this->serialDebug(2, "File system mounted\n");
    _fsInitialised = true;
  }

  if (_fsInitialised) {
    DynamicJsonDocument jsonConfig(CONFIG_MAX_FILE_SIZE);
    
    File configFile = LittleFS.open(_filename, "r");
    if (!configFile) {
      this->serialDebug(1, "Failed to open config file\n");
    } else {
      serialDebug(4, "Config file size: ");
      serialDebug(4, String(configFile.size()));
      serialDebug(4, " bytes\n");
      const size_t fileSize = configFile.size();
      if (fileSize > CONFIG_MAX_FILE_SIZE) {
        this->serialDebug(1, "Config file size is too large\n");
      } else if (fileSize == 0) {
        serialDebug(2, "No configuration file found");
      } else {
        // Allocate a buffer to store contents of the file.
        DeserializationError err = deserializeJson(jsonConfig, configFile);
        if (err) {
          this->serialDebug(1, "Unable to parse config file. Failed with code: ");
          this->serialDebug(1, err.c_str());
          this->serialDebug(1, "\n");
        } else {
          this->serialDebug(2, "Config loaded from " + _filename + "\n");
          String jsonString = this->toString(jsonConfig);
          this->serialDebug(3, jsonString);
          this->serialDebug(3, "\n");
          this->serialDebug(3, "Config length: ");
          this->serialDebug(3, String(jsonString.length()));
          this->serialDebug(3, " bytes\n");
          if (jsonString.length() > 0) {
            _md5 = this->md5(jsonString);
            this->serialDebug(3, "Config MD5: ");
            this->serialDebug(3, _md5);
            this->serialDebug(3, "\n");
          }

          // close the file
          configFile.close();
          
          // populate items
          serialDebug(3, "Attempting to parse json file\n");
          this->parseJson(jsonConfig);
          
          _fileLoaded = true;
        }
      }
    }
  }
}

DeviceConfig *DeviceConfig::parseJson(JsonDocument &jsonDoc) {
  // device properties
  if (jsonDoc.containsKey("device-name")) {
    deviceName = String((const char*) jsonDoc["device-name"]);
  }

  // blind properties
  if (jsonDoc.containsKey("blind")) {
    JsonObject blindConfig = jsonDoc["blind"];

    if (blindConfig.containsKey("current-position")) {
      blind.maxPosition = blindConfig["max-position"];
    }
    if (blindConfig.containsKey("reverse-rotation")) {
      blind.reverse = blindConfig["reverse-rotation"];
    }
  }
  
  // wifi properties
  if (jsonDoc.containsKey("access-point")) {
    // wifi ap properties
    if (jsonDoc.containsKey("access-point")) {
  
    }
  
    // wifi sta properties
    if (jsonDoc.containsKey("station")) {
  
    }
  }

  // update properties
  if (jsonDoc.containsKey("update")) {
    JsonObject updateConfig = jsonDoc["update"];

    if (updateConfig.containsKey("url")) {
       update.url = String((const char*) updateConfig["url"]);
    }
    if (updateConfig.containsKey("version-file")) {
      update.versionFile = String((const char*) updateConfig["version-file"]);
    }
    if (updateConfig.containsKey("interval")) {
      update.interval = updateConfig["interval"];
    }
    if (updateConfig.containsKey("retries")) {
      update.retries = updateConfig["retries"];
    }
    if (updateConfig.containsKey("auto-update")) {
      update.autoUpdate = updateConfig["auto-update"];
    }
  }

  return this;
}

/**
 * Saves the current position
 */
long DeviceConfig::savePosition(long position) {
  this->serialDebug(5, "Saving current position\n");

  if (!LittleFS.begin()) {
    this->serialDebug(1, "Failed to mount file system\n");
    _fsInitialised = false;
  } else {
    this->serialDebug(2, "File system mounted\n");
    _fsInitialised = true;
  }

  if (_fsInitialised) {
    File positionFile = LittleFS.open(_positionFilename, "w");
    if (!positionFile) {
      this->serialDebug(1, "Failed to open position file ");
      this->serialDebug(1, _positionFilename.c_str());
      this->serialDebug(1, "\n");
    }
    this->serialDebug(5, "Writing to position file\n");
    positionFile.write ((byte*)&position, sizeof(position));

    // close the file configFile.close();
    positionFile.close();
  }

  return position;
}
 
/**
 * Saves a config file t the file system. If the file exists, 
 * it is overwritten.
 */
DeviceConfig *DeviceConfig::saveConfig() {
  if (!LittleFS.begin()) {
    this->serialDebug(1, "Failed to mount file system\n");
    _fsInitialised = false;
  } else {
    this->serialDebug(2, "File system mounted\n");
    _fsInitialised = true;
  }

  if (_fsInitialised) {
    DynamicJsonDocument jsonConfig = toJson();
    String jsonString = this->toString(jsonConfig);
    String md5 = this->md5(jsonString);
    if (_md5.equals(md5)) {
      this->serialDebug(4, "Configuration hasn't changed, file not updated,\n");
    } else {
      this->serialDebug(2, "Config has changed, save to file\n");
      _md5 = md5;
      
      File configFile = LittleFS.open(_filename, "w");
      if (!configFile) {
        this->serialDebug(1, "Failed to open config file\n");
      }
    
      size_t fileSize = serializeJsonPretty(jsonConfig, configFile);
      this->serialDebug(2, "Wrote " + String(fileSize) + " bytes to config file " + _filename.c_str() + "\n");
      this->serialDebug(3, jsonString);
      this->serialDebug(3, "\n");
      this->serialDebug(3, "Config MD5: ");
      this->serialDebug(3, md5);
      this->serialDebug(3, "\n");

      // close the file configFile.close();
      configFile.close();
    }
  }

  return this;
}

DynamicJsonDocument DeviceConfig::toJson() {
  DynamicJsonDocument jsonConfig(CONFIG_MAX_FILE_SIZE);

  jsonConfig["device-name"] = deviceName;

  JsonObject blindConfig = jsonConfig.createNestedObject("blind");
  blindConfig["max-position"] = blind.maxPosition;
  blindConfig["reverse-rotation"] = blind.reverse;

  JsonObject wifiConfig = jsonConfig.createNestedObject("wifi");
  JsonObject apConfig = jsonConfig.createNestedObject("access-point");
  apConfig["ssid"] = wifi.ap_ssid;
  apConfig["password"] = wifi.ap_pwd;

  JsonObject updateConfig = jsonConfig.createNestedObject("update");
  updateConfig["url"] = update.url;
  updateConfig["version-file"] = update.versionFile;
  updateConfig["interval"] = update.interval;
  updateConfig["retries"] = update.retries;
  updateConfig["auto-update"] = update.autoUpdate;

  return jsonConfig;
}

bool DeviceConfig::hasChanged() {
    DynamicJsonDocument jsonConfig = toJson();
    String jsonString = this->toString(jsonConfig);
    String md5 = this->md5(jsonString);
    if (_md5.equals(md5)) {
      this->serialDebug(4, "Configuration hasn't changed from the saved version.\n");
      return false;
    } else {
      this->serialDebug(4, "Configuration has changed since it was last saved.\n");
      return true;
    }

}
/**
 * Utility functions
 * 
 */
 
/**
 * Private methods
 * 
 */
String DeviceConfig::toString(JsonDocument &jsonDoc) {
  String jsonString = "";
  serializeJsonPretty(jsonDoc, jsonString);
  return jsonString;
}

String DeviceConfig::md5(String str) {
  MD5Builder _builder;
  
  _builder.begin();
  _builder.add(String(str));
  _builder.calculate();
  return _builder.toString();
}

String DeviceConfig::getDeviceId() {
  // get the chip id
  char chipId[7];
  sprintf(chipId, "%06x", ESP.getChipId());
  return String(chipId);
}

void DeviceConfig::serialDebug(int level, String message) {
  if (debugLevel >= level) {
    Serial.print(message);
  }
}
