# blind

An ESP8266 based blind controller.

The controller is based on the [motor on roller blind ws](https://github.com/nidayand/motor-on-roller-blind-ws) project. The blind hardware can be found on [Thingiverse](https://www.thingiverse.com/thing:2392856).

**Changes from the original** (some are still a work in progress):
- New UI
- Updated WiFi manager, allows the reconfiguration of wifi settings using the main UI
- Online updates
- Automatic update option
- Integration with Google Home Assistant via the [DumbDevices](https://dumbdevices.web.app) device manager.

**Known issues** (these will be fixed in later releases):
- Still uses the original WiFi manager, so networking is not as seamless as it could be, also captive portal can only be used to configure wifi
- Updates require internet access, no file upload functionality available yet
- DumbDevices integration is not yet implimented (requires replacement of original MQTT client)

**How to install**
1. From code. The project can be built using the Arduino IDE and flashed to the device.
2. From binary. The prebuilt releases can be flashed to the device using the [NodeMCU PyFlasher](https://github.com/marcelstoer/nodemcu-pyflasher)
3. Online updates. After the initial flashing, the device can update itself if it has an internet connection.
