#include "WifiManager.h"

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>

WifiManager::WifiManager(DeviceConfig &config) {
  serialDebug(2, "Initialising WiFiHandler\n");
  deviceConfig = &config;
}

void WifiManager::begin() {
    WiFi.mode(WIFI_STA);

    //set static IP if entered
    if (WiFi.SSID() != "")
    {
      serialDebug(3, "Connecting to previous WiFi network\n");
      //trying to fix connection in progress hanging
      ETS_UART_INTR_DISABLE();
      wifi_station_disconnect();
      ETS_UART_INTR_ENABLE();
      WiFi.begin();
    }

    if (WiFi.waitForConnectResult() == WL_CONNECTED)
    {
      startDNS();
      //connected
      serialDebug(3, "Connected to stored WiFi details\n");
      serialDebug(3, "IP address: ");
      serialDebug(3, WiFi.localIP().toString());
      serialDebug(3, "\n");
    }
    else
    {
      serialDebug(2, "No wifi configured, starting access point\n");
      startAccessPoint();
    }
}

void WifiManager::startAccessPoint() {
  WiFi.persistent(false);
  // disconnect sta, start ap
  WiFi.disconnect(); //  this alone is not enough to stop the autoconnecter
  WiFi.mode(WIFI_AP);
  WiFi.persistent(true);

  WiFi.softAP(deviceConfig->wifi.ap_ssid, deviceConfig->wifi.ap_pwd);

//  startDNS();
  dnsServer = new DNSServer();

  /* Setup the DNS server redirecting all the domains to the apIP */
  dnsServer->setErrorReplyCode(DNSReplyCode::NoError);
  dnsServer->start(53, "*", WiFi.softAPIP());

  serialDebug(2, "Access point " + deviceConfig->wifi.ap_ssid + " started\n");
  serialDebug(2, "IP address: ");
  serialDebug(2, WiFi.softAPIP().toString());
  serialDebug(2, "\n");
  
  isAccessPoint = true;
}

//function to stop the captive portal
void WifiManager::stopAccessPoint()
{    
  WiFi.mode(WIFI_STA);
  delete dnsServer;

  isAccessPoint = false;    
}

//function to forget current WiFi details and start a captive portal
void WifiManager::forget()
{ 
  MDNS.close();
  WiFi.disconnect();
//  startAccessPoint();

  serialDebug(3, PSTR("Requested to forget WiFi. Started Captive portal."));
  ESP.restart();
}

//function to request a connection to new WiFi credentials
void WifiManager::setNewWifi(String newSSID, String newPass)
{
  ssid = newSSID;
  pass = newPass;

  reconnect = true;
}

//function to connect to new WiFi credentials
void WifiManager::connectNewWifi(String newSSID, String newPass)
{
  delay(1000);

  //set static IP or zeros if undefined    
//    WiFi.config(ip, gw, sub, dns);

  //fix for auto connect racing issue
  if (!(WiFi.status() == WL_CONNECTED && (WiFi.SSID() == newSSID)))
  {
    //trying to fix connection in progress hanging
    ETS_UART_INTR_DISABLE();
    wifi_station_disconnect();
    ETS_UART_INTR_ENABLE();

    //store old data in case new network is wrong
    String oldSSID = WiFi.SSID();
    String oldPSK = WiFi.psk();

    WiFi.begin(newSSID.c_str(), newPass.c_str(), 0, NULL, true);
    delay(2000);

    if (WiFi.waitForConnectResult() != WL_CONNECTED)
    {
      serialDebug(1, PSTR("New connection unsuccessful"));
      if (!isAccessPoint)
      {
        WiFi.begin(oldSSID, oldPSK, 0, NULL, true);
        if (WiFi.waitForConnectResult() != WL_CONNECTED)
        {
          serialDebug(1, PSTR("Reconnection failed too"));
          startAccessPoint();
        }
        else 
        {
          serialDebug(2, PSTR("Reconnection successful"));
          serialDebug(2, WiFi.localIP().toString());
//          startDNS();
          ESP.restart();
        }
      }
    }
    else
    {
      if (isAccessPoint)
      {
        stopAccessPoint();
      }

      Serial.println(PSTR("New connection successful"));
      Serial.println(WiFi.localIP());
      ESP.restart();
    }
  }
}

//captive portal loop
void WifiManager::loop()
{
  if (isAccessPoint)
  {
    //captive portal loop
    dnsServer->processNextRequest();
  } else {
    MDNS.update();
  }

  if (reconnect)
  {
    connectNewWifi(ssid, pass);
    reconnect = false;
  }    
}

void WifiManager::updateHostname() {
  if (!isAccessPoint) {
    startDNS();
  }
}

void WifiManager::startDNS() {
  MDNS.close();
  if (!MDNS.begin(deviceConfig->deviceName)) {
    Serial.println("Error setting up MDNS responder!");
  }
  MDNS.addService("http", "tcp", 80);
  MDNS.addService("ws", "tcp", 81);
  Serial.println("mDNS responder started");
}

void WifiManager::serialDebug(int debugLevel, String message) {
  if (debugLevel <= _debugLevel) {
    Serial.print(message);
  }
}
