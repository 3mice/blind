#ifndef ConfigManager_h
#define ConfigManager_h

#include <ArduinoJson.h>
#include <MD5Builder.h>

#define CONFIG_MAX_FILE_SIZE 1024

#ifndef CONFIG_MANAGER_DEBUG
#define CONFIG_MANAGER_DEBUG 2
#endif

/**
 * The 3mice config handler.
 * 
 * Manages the loading and saving of the config file. Config file is returned as an
 * ArduinoJSON document.
 */
// blind properties
class BlindConfig {
  public:
    long maxPosition = 10000;
    bool reverse = false;
  private:
};

class WiFiConfig {
  public:
    // wifi ap properties
    String ap_ssid = "3mice-{ID}";                  //Name of access point
    String ap_pwd  = "3mice-blind";             //Hardcoded password for access point

  private:
};

class UpdateConfig {
  public:
    // update properties
    String url = "http://raspberrypi.local/firmware/";
    String versionFile = "firmware.version";
    long interval = 1000 * 60 * 5;            // check every 5 minutes
    int retries = 3;
    bool autoUpdate = true;
    
  private:
};

class DeviceConfig {
  public:
    // constructors
    
    // builders
    DeviceConfig *init(long version, String filename);
    DeviceConfig *loadConfig();
    long loadPosition(long defaultPosition);
    DeviceConfig *saveConfig();
    long savePosition(long position);
    DeviceConfig *parseJson(JsonDocument &jsonDoc);
    DynamicJsonDocument toJson();
    bool hasChanged();

    // device properties
    String deviceName = "blind-{ID}";
    String deviceId;
    long   firmwareVersion;
    String deviceType = "blind";

    // debug
    int debugLevel = CONFIG_MANAGER_DEBUG;

    BlindConfig blind;
    WiFiConfig wifi;
    UpdateConfig update;
    
  private:
    // config manager properties
    String _filename;
    bool   _fileLoaded = false;
    String _md5 = "";
    bool _fsInitialised = false;


    String toString(JsonDocument &jsonDoc);
    String md5(String source);
    String getDeviceId();

    String _positionFilename = "current.pos";
    void serialDebug(int level, String message);
};

#endif
