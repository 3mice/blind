const char WEB_UI[] PROGMEM = R"(<html>
  <head>
    <title>3mice blind controller</title>
    <style>
      html, body, .grid-container { height: 100%; margin: 0; }
      .grid-container {
        display: grid;
        grid-template-columns: 1fr 3fr;
        grid-template-rows: auto 1fr auto;
        grid-gap: 5px 5px;
        grid-template-areas:
          'header header'
          'menu main'
          'footer footer';
      }
      .grid-container * {
        border: #6495ed;
        position: relative;
      }
      .grid-header {
        grid-area: header;
      }
      .grid-menu {
        list-style: none;
        grid-area: menu;
      }
      .grid-menu-list {
        list-style: none;
      }
      .grid-main {
        grid-area: main;
        overflow: auto;
      }
      .grid-footer {
        display: none;
        grid-area: footer;
        background-color: LightBlue;
        padding-left: 10px;
      }
      .grid-footer-error {
        display: none;
        grid-area: footer;
        background-color: rgba(255, 99, 71, 0.5);
        padding-left: 10px;
      }
      .title {
        text-align: center;
      }
      .byline {
        text-align: center;
      }
      li a {
          display: block;
          color: white;
          padding: 8px 0 8px 16px;
          margin: 0 0 1px 0;
          text-decoration: none;
          background-color: #6495ed;
      }
      li a.group {
          background-color: #6495ed;
          color: white;
          font-weight: bold;
      }
      li a:hover:not(.group) {
          background-color: #0000ff;
          color: white;
      }
      li a.current {
          background-color: #0000ff;
          color: white;
      }
      .position-grid {
        display: grid;
        grid-template-columns: auto 1fr auto 5px;
        grid-template-rows: auto;
        grid-gap: 7px 7px;
        grid-template-areas:
          'pos-up pos-slide pos-down .';
        align-items: center;
      }
      .pos-up {
        grid-area: pos-up;
      }
      .pos-slide {
        grid-area: pos-slide;
      }
      .btn-down {
        grid-area: pos-down;
      }
      .position-slider {
        -webkit-appearance: none;
        width: 100%;
        height: 15px;
        border-radius: 5px;
        background: #d3d3d3;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
        justify-content: center;
        align-items: center;
      }
      .position-slider:hover {
        opacity: 1;
      }
      .position-slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        background: DodgerBlue;
        cursor: pointer;
      }
      .position-slider::-moz-range-thumb {
        width: 25px;
        height: 25px;
        border-radius: 50%;
        background: DodgerBlue;
        cursor: pointer;
      }
      .calibrate-grid {
        display: grid;
        grid-template-columns: 1fr 1fr 1fr 5px;
        grid-template-rows: auto auto auto auto;
        grid-gap: 7px 7px;
        grid-template-areas:
          'cal-up cal-stop cal-down .'
          'cal-min . cal-max .'
          '. cal-dir . .'
          '. . . .';
      }
      .cal-up {
        grid-area: cal-up;
      }
      .cal-stop {
        grid-area: cal-stop;
      }
      .cal-down {
        grid-area: cal-down;
      }
      .cal-min {
        grid-area: cal-min;
      }
      .cal-max {
        grid-area: cal-max;
      }
      .cal-dir {
        grid-area: cal-dir;
      }
      .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
      }
      .switch input {
        opacity: 0;
        width: 0;
        height: 0;
      }
      .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
      }
      .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
      }
      input:checked + .slider {
        background-color: #2196F3;
      }
      input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
      }
      input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
      }
      .slider.round {
        border-radius: 34px;
      }
      .slider.round:before {
        border-radius: 50%;
      }
      .wifi-grid {
        display: grid;
        grid-template-columns: 1fr 2fr 8fr 5px;
        grid-template-rows: auto auto auto auto auto;
        grid-gap: 7px 7px;
        grid-template-areas:
          'wifi-ssid-icon wifi-ssid-label wifi-ssid.'
          'wifi-pwd-icon wifi-pwd-label wifi-pwd .'
          'wifi-host-icon wifi-host-label wifi-host .'
          '. wifi-btns wifi-btns .'
          '. . . .';
      }
      .wifi-ssid-icon {
        grid-area: wifi-ssid-icon;
      }
      .wifi-ssid-label {
        grid-area: wifi-ssid-label;
        justify-self: start;
      }
      .wifi-ssid {
        grid-area: wifi-ssid;
        justify-self: start;
      }
      .wifi-pwd-icon {
        grid-area: wifi-pwd-icon;
      }
      .wifi-pwd-label {
        grid-area: wifi-pwd-label;
        justify-self: start;
      }
      .wifi-pwd {
        grid-area: wifi-pwd;
        justify-self: start;
      }
      .wifi-host-icon {
        grid-area: wifi-host-icon;
      }
      .wifi-host-label {
        grid-area: wifi-host-label;
        justify-self: start;
      }
      .wifi-host {
        grid-area: wifi-host;
        justify-self: start;
      }
      .wifi-btns {
        grid-area: wifi-btns;
        justify-self: start;
      }
      .wifi-btn-grid {
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;
        grid-gap: 7px 7px;
        grid-template-areas:
          '. wifi-btn-clear wifi-btn-apply'
      }
      .wifi-btn-clear {
        grid-area: wifi-btn-clear;
      }
      .wifi-btn-apply {
        grid-area: wifi-btn-apply;
      }
      .btn {
        background-color: DodgerBlue; /* Blue background */
        border: none; /* Remove borders */
        color: white; /* White text */
        padding: 12px 16px; /* Some padding */
        font-size: 16px; /* Set a font size */
        cursor: pointer; /* Mouse pointer on hover */
      }
      .btn:hover {
        background-color: RoyalBlue;
      }
      .btn-disabled {
        background-color: LightGray; /* Blue background */
        border: none; /* Remove borders */
        color: white; /* White text */
        padding: 12px 16px; /* Some padding */
        font-size: 16px; /* Set a font size */
      }
      .mnu {
        background-color: White; /* Blue background */
        border: none; /* Remove borders */
        padding: 12px 16px; /* Some padding */
        font-size: 16px; /* Set a font size */
        cursor: pointer; /* Mouse pointer on hover */
        width: 100%;
        text-align: left;
      }
      .mnu:hover {
        background-color: DodgerBlue;
        color: white;
      }
      .main-home {
        display: block;
      }
      .main-calibrate {
        display: none;
      }
      .main-smart {
        display: none;
      }
      .main-wifi {
        display: none;
      }
      .main-update {
        display: none;
      }
      .main-about {
        display: none;
      }
      section {
        display: flex;
        justify-content: center;
        align-items: center;
      }
    </style>
    <script>
      // svg Instructions
      var iconUp = "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' viewBox='0 0 16 16'><path d='M7.247 4.86l-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z'/></svg>";
      var iconDown = "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' viewBox='0 0 16 16'><path d='M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z'/></svg>";
      var iconStop = "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' viewBox='0 0 16 16'><path d='M5 3.5h6A1.5 1.5 0 0 1 12.5 5v6a1.5 1.5 0 0 1-1.5 1.5H5A1.5 1.5 0 0 1 3.5 11V5A1.5 1.5 0 0 1 5 3.5z'/></svg>";

      // ws connection
      const wsUrl = 'ws://' + location.host + ':81/';
      var hostname = "";
      var ssid = "";

      var gotoPos = function(percent) {
        doSend(percent);
      }
      var instr = function(action) {
        doSend('(' + action + ')');
      }

      var websocket;
      var timeOut;
      function retry(){
        clearTimeout(timeOut);
        timeOut = setTimeout(function(){
          websocket=null; init();},5000);
      };
      function setIcon(iconClass, iconSvg) {
        var x = document.getElementsByClassName(iconClass);
        var i;
        for (i = 0; i < x.length; i++) {
          x[i].innerHTML = iconSvg;
        }
      };
      function init(){
        console.log('Setting icons');
        setIcon('icon-up', iconUp);
        setIcon('icon-down', iconDown);
        setIcon('icon-stop', iconStop);

        console.log('Connecting to ' + wsUrl + ' ...');
        try{
          websocket = new WebSocket(wsUrl);
          websocket.onclose = function () {};
          websocket.onerror = function(evt) {
            console.log('Cannot connect to device');
            document.getElementById('ws-error').style.display = 'block';
            retry();
          };
          websocket.onopen = function(evt) {
            console.log('Connected to device');
            document.getElementById('ws-error').style.display = 'none';
            setTimeout(function(){doSend('(update)');}, 1000);
          };
          websocket.onclose = function(evt) {
            console.log('Disconnected. Retrying');
            document.getElementById('ws-error').style.display = 'block';
            retry();
          };
          websocket.onmessage = function(evt) {
            try{
              var msg = JSON.parse(evt.data);
              console.log("Received [" + JSON.stringify(msg) + "]");
              if (typeof msg.set !== 'undefined'){
                console.log("Setting position to [" + msg.set + "]");
                document.getElementById('openPercent').value = msg.set;
              };
              // wifi settings
              if (typeof msg.wifi_status !== 'undefined'){
                console.log("Setting wifi status to [" + msg.wifi_status + "]");
                document.getElementById('wifi-status').innerHTML = "<b>Status: </b>" + msg.wifi_status;
              };
              if (typeof msg.wifi_ssid !== 'undefined'){
                console.log("Setting wifi ssid to [" + msg.wifi_ssid + "]");
                document.getElementById('sta-ssid').value = msg.wifi_ssid;
                document.getElementById('sta-password').value = "";
                ssid = msg.wifi_ssid;
                updateWifiButtons();
              };
              if (typeof msg.hostname !== 'undefined'){
                console.log("Setting hostname to [" + msg.hostname + "]");
                document.getElementById('title').innerHTML = "<h1>" + msg.hostname + "</h1>";
                document.getElementById('hostname').value = msg.hostname;
                hostname = msg.hostname;
              };
              if (typeof msg.firmware !== 'undefined'){
                console.log("Setting firmware version to [" + msg.firmware + "]");
                document.getElementById('firmware').innerHTML = "<p>Current firmware version: " + msg.firmware + "</p>";
              };
              if (typeof msg.reverse !== 'undefined'){
                console.log("Setting reverse directon to [" + msg.reverse + "]");
                document.getElementById('reverse').checked = msg.reverse;
              };
            } catch(err){
              console.log('Error decoding response: ');
              console.log(err);
            }
          };
        } catch (e){
          console.log('Cannot connect to device. Retrying...');
          console.log(e);
          retry();
        };
      };
      function doSend(msg){
        if (websocket && websocket.readyState == 1){
          console.log("Sending message [" + msg + "]");
          websocket.send(msg);
        }
      };
      window.addEventListener('load', init, false);
      window.onbeforeunload = function() {
        if (websocket && websocket.readyState == 1){
          websocket.close();
        };
      };

      function updateWifiButtons() {
        var canApply = false;
        var canForget = true;
        // validate hostname
        var x = document.getElementById('sta-ssid');
        if (x.value != "") {
          if (x.value != ssid) {
            canApply = true;
            canForget = false;
          }
        } else {
          canForget = false;
        }
        var x = document.getElementById('sta-password');
        if (x.value != "") {
          canApply = true;
        }
        var x = document.getElementById('hostname');
        if (x.value != "" && x.value != hostname) {
          canApply = true;
        }

        x = document.getElementById('btn-forget');
        if (canForget) {
          x.className = 'btn';
          x.disabled = false;
        } else {
          x.className = 'btn-disabled';
          x.disabled = true;
        }
        x = document.getElementById('btn-apply');
        if (canApply) {
          x.className = 'btn';
          x.disabled = false;
        } else {
          x.className = 'btn-disabled';
          x.disabled = true;
        }
      };
      function sendDirection() {
        var reverseDir = document.getElementById('reverse').checked;
        if (reverseDir) {
          doSend('(reverse)');
        } else {
          doSend('(forward)');
        }
      };
      function connectWifi() {
        var wifi_ssid = document.getElementById('sta-ssid').value.trim();
        var wifi_pwd = document.getElementById('sta-password').value.trim();
        var wifi_host = document.getElementById('hostname').value.trim();
        doSend('(connect ssid="' + wifi_ssid +
          '", password="' + wifi_pwd +
          '", hostname="' + wifi_host + '")');
      };
      // page functions
      function hideAllPages() {
        var x = document.getElementsByClassName('menu-page');
        var i;
        for (i = 0; i < x.length; i++) {
          x[i].style.display = 'none';
        }
      }
      function showPage(page) {
        var x = document.getElementById(page);
        x.style.display = 'block';
      }
      function setPage(target) {
        hideAllPages();
        showPage(target);
      }
    </script>
  </head>
  <body>
    <div class='grid-container'>
      <div class='grid-header'>
        <div id='title' class='title'><h1>3mice device</h1></div>
        <div class='byline'><p>Blind controller</p></div>
      </div>
      <div class='grid-menu'>
        <ul class='grid-menu-list'>
          <li><button class='mnu' onClick='setPage("page-home");'>Home</button></li>
          <li><button class='mnu' onClick='setPage("page-calibrate");'>Calibrate</button></li>
          <!--<li><button class='mnu' onClick='setPage("page-smart");'>Smart Home</button></li>-->
          <li><button class='mnu' onClick='setPage("page-wifi");instr("wifi");'>WiFi</button></li>
          <li><button class='mnu' onClick='setPage("page-update");'>Update</button></li>
          <li><button class='mnu' onClick='setPage("page-about");'>About</button></li>
        </ul>
      </div>
      <div class='grid-main'>
        <div id='page-home' class='menu-page main-home'>
          <div><h2>Adjust position<h2></div>
          <div><p>Move the slider to the desired position or use the arrows to open/close the blind completely.</p></div>
          <div class='position-grid'>
            <div class='pos-up'>
              <section>
                <button class='btn' onClick='gotoPos(0)'><i class='icon-up'></i></button>
              </section>
            </div>
            <div class='pos-slide'>
              <section>
                <input class='position-slider' type='range' min='0' max='100' id='openPercent' onInput='gotoPos(this.value)'>
              </section>
            </div>
            <div class='pos-down'>
              <section>
                <button class='btn' onClick='gotoPos(100)'><i class='icon-down'></i></button>
              </section>
            </div>
          </div>
        </div>
        <div id='page-calibrate' class='menu-page main-calibrate'>
          <div><h2>Calibrate blind<h2></div>
          <div><h3>Instructions<h3></div>
          <div class='content'>
            <p>
              <ol>
                <li>Use the arrow and stop buttons to navigate to the top position i.e. the blind is opened</li>
                <li>Click the MIN button</li>
                <li>Use the down arrow to navigate to the max closed position</li>
                <li>Click the MAX button</li>
                <li>If required, you can reverse the default rotation direction</li>
                <li>Calibration is completed!</li>
              </ol>
            </p>
          </div>
          <div class='calibrate-grid'>
            <div class='cal-up'>
              <section>
                <button class='btn' onClick='instr("-1")'><i class="icon-up"></i></button>
              </section>
            </div>
            <div class='cal-stop'>
              <section>
                <button class='btn' onClick='instr("0")'><i class="icon-stop"></i></button>
              </section>
            </div>
            <div class='cal-down'>
              <section>
                <button class='btn' onClick='instr("1")'><i class="icon-down"></i></button>
              </section>
            </div>
            <div class='cal-min'>
              <section>
                <button class='btn' onClick='instr("min")'>MIN</button>
              </section>
            </div>
            <div class='cal-max'>
              <section>
                <button class='btn' onClick='instr("max")'>MAX</button>
              </section>
            </div>
            <div class='cal-dir'>
              <section>
                Reverse rotation directon
                <label class="switch">
                  <input type="checkbox" id="reverse" onclick='sendDirection()'>
                  <span class="slider round"></span>
                </label>
              </section>
            </div>
          </div>
        </div>
        <div id='page-wifi' class='menu-page main-wifi'>
          <div><h2>WiFi setup<h2></div>
          <div id="wifi-status"><b>Status: </b>...</div>
          <p>
          <div class='wifi-grid'>
            <div class='wifi-ssid-label'>
              <section>
                <label for='sta-ssid'>SSID: </label>
              </section>
            </div>
            <div class='wifi-ssid'>
              <section>
                <input type='text' id='sta-ssid' name='sta-ssid' autoCapitalize="none" oninput='updateWifiButtons()'>
              </section>
            </div>
            <div class='wifi-pwd-label'>
              <section>
              <label for='sta-password'>Password: </label>
            </section>
            </div>
            <div class='wifi-pwd'>
              <section>
                <input type='password' id='sta-password' name='sta-password' autoCapitalize="none" oninput='updateWifiButtons()'>
              </section>
            </div>
            <div class='wifi-host-label'>
              <section>
                <label for='hostname'>Hostname: </label>
            </section>
            </div>
            <div class='wifi-host'>
              <section>
                <input type='text' id='hostname' name='hotname' autoCapitalize="none" oninput='updateWifiButtons()'>
              </section>
            </div>
            <div class='wifi-btns'>
              <div class='wifi-btn-grid'>
                <div class='wifi-btn-clear'>
                  <button id='btn-forget' class='btn-disabled' onClick='instr("forget-wifi")' disabled>Forget</button>
                </div>
                <div class='wifi-btn-apply'>
                  <button id='btn-apply' class='btn-disabled' onClick='connectWifi()'>Apply</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id='page-smart' class='menu-page main-smart'>
          <div><h2>Smart Home setup<h2></div>
          <p>This feature is scheduled for a future release. Please check again after the next firmware update.</p>
        </div>
        <div id='page-update' class='menu-page main-update'>
          <div><h2>Update<h2></div>
          <div id='firmware'><p>Current firmware version: Unknown</p></div>
          <form method='POST' action='/update' enctype='multipart/form-data'>
            Firmware:<br>
            <input type='file' accept='.bin,.bin.gz' name='firmware'>
            <input type='submit' value='Update Firmware' class='btn'>
          </form>
          <!--
          <p><button class='btn'>Check for updates</button></p>
          <p><input type='checkbox' id='autoUpdate'>
          <label for='vehicle1'>Update automatically</label></p>
          -->
        </div>
        <div id='page-about' class='menu-page main-about'>
          <div><h2>About<h2></div>
          <p>An automated blind controller using the ESP8266.</p>
          <p>Firmware features:
            <ul>
              <li>Configurable WiFi connection</li>
              <li>Online updates, easier to update remote devices</li>
              <!--
              <li>Supports Google Assistant voice control via DumbDevices</li>
              -->
            </ul>
          </p>
          <p>Visit <a href='https://three-mice.web.app' target='_blank'>3mice</a> for other device controllers.</p>
        </div>
      </div>
      <div id='ws-error' class='grid-footer-error'>
        <div><h3><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
          <path d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435zm1.4.7a.495.495 0 0 0-.7 0L1.134 7.65a.495.495 0 0 0 0 .7l6.516 6.516a.495.495 0 0 0 .7 0l6.516-6.516a.495.495 0 0 0 0-.7L8.35 1.134z"/>
          <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
        </svg>&nbsp;No connection to device, you will not be able to make changes to the device at this time.</h3></div>
      </div>
    </div>
  </body>
</html>
)";
