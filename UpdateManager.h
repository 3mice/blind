#ifndef UploadManager_h
#define UploadManager_h

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include "ConfigManager.h"

#ifndef UPDATE_MANAGER_DEBUG
#define UPDATE_MANAGER_DEBUG 2
#endif

/**
 * The 3mice updater.
 * 
 * Provides the following update types:
 * 1. HTTP upload
 * 
 * Will provide the following update types:
 * 2. HTTP server - complex server
 * 3. Configurable auto/manual updates for HTTP server
 * 4. Secure/signed updater
 */
class UpdateManager {
  public:
    UpdateManager(DeviceConfig config, ESP8266WebServer *server);
    void begin();

    void checkForUpdates(DeviceConfig &config);
    void configure(DeviceConfig &config);
    void update();
    
    // debug
    int _debugLevel = UPDATE_MANAGER_DEBUG;

  private:
    DeviceConfig _config;
    ESP8266WebServer *_server;
    ESP8266HTTPUpdateServer _httpUpdater;

    void serialDebug(int debugLevel, String message);
};

#endif
