#include "UpdateManager.h"

// For FOTA
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

UpdateManager::UpdateManager(DeviceConfig config, ESP8266WebServer *server) {
  _server = server;
}

void UpdateManager::begin() {
  serialDebug(2,"Starting update server\n");
  _httpUpdater.setup(_server);
}

void UpdateManager::serialDebug(int debugLevel, String message) {
  if (debugLevel <= _debugLevel) {
    Serial.print(message);
  }
}

void UpdateManager::configure(DeviceConfig &config) {
}

void UpdateManager::update() {
//  long checkVersionTime;
  
//  if (millis() > (checkVersionTime + config.updateInterval)) {
//      checkForUpdates();
//      checkVersionTime = millis();
//  }
}

void UpdateManager::checkForUpdates(DeviceConfig &config) {
  // TODO include time checking (with rollover of millis?
  
  String fwUrl = String(config.update.url);
  fwUrl.concat(config.deviceType);
  fwUrl.concat("/");
  String fwVersionUrl = fwUrl;
  fwVersionUrl.concat(config.update.versionFile);

  this->serialDebug(2, "\n");
  this->serialDebug(2, "Checking for firmware updates\n");
  this->serialDebug(2, "Firmware version url: ");
  this->serialDebug(2, fwVersionUrl);
  this->serialDebug(2, "\n");

  HTTPClient httpClient;
  int attempts = 0;
  int httpCode = -1;
  httpClient.begin(fwVersionUrl);
  while (attempts < config.update.retries && httpCode < 0) {
    serialDebug(2, "Checking latest firmware version.");
    httpCode = httpClient.GET();
    attempts++;
  }
  if (httpCode == 200) {
    String newFwVersion = httpClient.getString();
    newFwVersion.trim();
    serialDebug(2,"Current firmware version: ");
    serialDebug(2,String(config.firmwareVersion));
    serialDebug(2,"New firmware version: ");
    serialDebug(2,newFwVersion);

    int newVersion = newFwVersion.toInt();
    if (newVersion > config.firmwareVersion) {
      serialDebug(2,"Preparing to update.");
      String fwImageUrl = fwUrl;
      fwImageUrl.concat(newFwVersion);
      fwImageUrl.concat(".bin");
      serialDebug(2,"Fetching firmware image: ");
      serialDebug(2,fwImageUrl);

      t_httpUpdate_return ret = ESPhttpUpdate.update(fwImageUrl);
      switch (ret) {
        case HTTP_UPDATE_FAILED: 
          serialDebug(1,"HTTP_UPDATE_FAILED Error (");
          serialDebug(1,String(ESPhttpUpdate.getLastError())); 
          serialDebug(1,"): "); 
          serialDebug(1,ESPhttpUpdate.getLastErrorString().c_str());
          serialDebug(1,"\n"); 
          break;
        case HTTP_UPDATE_NO_UPDATES:
          serialDebug(2,"HTTP_UPDATE_NO_UPDATES");
          break;
      }
    } else {
      serialDebug(2,"Already on latest version.");
    }
  } else {
    serialDebug(2,"Firmware version check failed, got HTTP response code: ");
    serialDebug(2,String(httpCode));
  }
  httpClient.end();
}
