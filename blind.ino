// Set initial debug levels
#ifndef CONFIG_MANAGER_DEBUG
#define CONFIG_MANAGER_DEBUG 2
#endif
#ifndef WIFI_MANAGER_DEBUG
#define WIFI_MANAGER_DEBUG 4
#endif

#include <Stepper_28BYJ_48.h>
#include <ESP8266WebServer.h>
#include <WebSocketsServer.h>
#include "web_ui.h"
#include "WifiManager.h"
#include "UpdateManager.h"
#include "ConfigManager.h"

#define FW_VERSION 2021011735

#define CONFIG_FILE "/config.json"

// keep track of the blind operation
String action;                      //Action manual/auto
long path = 0;                      //Direction of blind (1 = down, 0 = stop, -1 = up)
long setPos = 0;                    //The set position 0-100% by the client
int ledPulsePos = 0;

boolean firstRun = true;            //To enable actions first time the loop is run
boolean saveConfig = false;
boolean savePosition = false;
long currentPosition = 0;

// motor configuration, move to config in future
Stepper_28BYJ_48 small_stepper(D1, D3, D2, D4); //Initiate stepper driver


// servers
DeviceConfig config;
//(FW_DEVICE_TYPE, FW_VERSION, CONFIG_FILE);
WifiManager wifiManager(config);
ESP8266WebServer server(80);              // TCP server at port 80 will respond to HTTP requests
WebSocketsServer webSocket = WebSocketsServer(81);  // WebSockets will respond on port 81
UpdateManager updateManager(config, &server);


/****************************************************************************************
*/
void websocketSend(uint8_t clientnum, String message) {  
  if (clientnum = 255) {
    Serial.println("Broadcasting message to all clients");
    webSocket.broadcastTXT(message);    
  } else {
    Serial.print("Sending message to client ");
    Serial.println(clientnum);
    webSocket.sendTXT(clientnum, message);  
  }
}

void reportPosition(uint8_t clientnum) {  
  int set = (setPos * 100)/config.blind.maxPosition;
  int pos = (currentPosition * 100)/config.blind.maxPosition;
  String response = "{ \"set\":" + String(set) + 
    ", \"position\":" + String(pos) +
    ", \"reverse\":" + String(config.blind.reverse) +" }";
  Serial.print("Reporting position ");
  Serial.println(response.c_str());

  websocketSend(clientnum, response);
}

void reportWifiStatus(uint8_t clientnum) {
  String response = "{ \"wifi_status\":\"" + getWifiStatusString() + 
    "\", \"wifi_ssid\":\"" + WiFi.SSID() + 
    "\", \"hostname\":\"" + config.deviceName + "\" }";
  webSocket.sendTXT(clientnum, response); 

  websocketSend(clientnum, response);
}

String getWifiStatusString() {
  switch (WiFi.status()) {
    case WL_CONNECTED:
      return "Connected";
    case WL_DISCONNECTED:
      return "Disconnected";
    default:
      return "Error (" + String(WiFi.status()) + ")";
  }
}

String getSubstring(String source, String key) {
  int start = source.indexOf(key);
  if (start >= 0) {
    start = source.indexOf("\"", start);
    if (start > 0) {
      int end = source.indexOf("\"", start+1);
      if (end > 0) {
        return source.substring(start+1, end);
      }
    }
  }

  return "";
}

/**
 * Handle control actions
 */
void processMsg(String res, uint8_t clientnum){
  /*
     Check if calibration is running and if stop is received. Store the location
  */
  if (action.equals("set") && res.equals("(0)")) {
    Serial.println("Calibration stopped");
    config.blind.maxPosition = currentPosition;
    saveConfig = true;
  }

  /*
     Below are actions based on inbound MQTT payload
  */
  if (res.equals("(min)")) {
    /*
       Store the current position as the start position
    */
    currentPosition = 0;
    path = 0;
    saveConfig = true;
    action = "manual";
  } else if (res.equals("(max)")) {
    /*
       Store the max position of a closed blind
    */
    config.blind.maxPosition = currentPosition;
    path = 0;
    saveConfig = true;
    action = "manual";
  } else if (res.equals("(0)")) {
    /*
       Stop
    */
    path = 0;
    savePosition = true;
    action = "manual";
  } else if (res.equals("(1)")) {
    /*
       Move down without limit to max position
    */
    path = 1;
    action = "manual";
  } else if (res.equals("(-1)")) {
    /*
       Move up without limit to top position
    */
    path = -1;
    action = "manual";
  } else if (res.equals("(update)")) {
    // initialise and page variables
    String response = "{ \"firmware\":\"" + String(FW_VERSION) + 
      "\", \"hostname\":\"" + config.deviceName + "\" }";
    webSocket.sendTXT(clientnum, response); 
    //Send position details to client
    reportPosition(clientnum);
  } else if (res.equals("(ping)")) {
    //Do nothing
  } else if (res.equals("(reverse)")) {
    config.blind.reverse = true;
    saveConfig = true;
    reportPosition(clientnum);
  } else if (res.equals("(forward)")) {
    config.blind.reverse = false;
    saveConfig = true;
    reportPosition(clientnum);
  } else if (res.equals("(forget-wifi)")) {
    wifiManager.forget();
    reportWifiStatus(clientnum);
  } else if (res.equals("(wifi)")) {
    reportWifiStatus(clientnum);
  } else if (res.startsWith("(connect ")) {
    String ssid = getSubstring(res, "ssid");
    String pwd = getSubstring(res, "password");
    String host = getSubstring(res, "hostname");

    if (host.length() > 0) {
      config.deviceName = host;
      config.saveConfig();
      wifiManager.updateHostname();
    }
    if (ssid.length() == 0 || pwd.length() == 0) {
      reportWifiStatus(255);
    } else {
      wifiManager.setNewWifi(ssid, pwd);
    }
  } else {
    /*
       Any other message will take the blind to a position
       Incoming value = 0-100
       path is now the position
    */
    path = config.blind.maxPosition * res.toInt() / 100;
    setPos = path; //Copy path for responding to updates
    action = "auto";

    //Send the instruction to all connected devices
    reportPosition(255);
  }
}

/**
 * Handle web socket messages from the web ui
 */
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
  switch(type) {
    case WStype_TEXT:
      Serial.printf("[%u] get Text: %s\n", num, payload);

      String res = String((char*)payload);

      //Send to common MQTT and websocket function
      processMsg(res, num);
      break;
  }
}

/**
  Turn of power to coils whenever the blind
  is not moving
*/
void stopPowerToCoils() {
  digitalWrite(D1, LOW);
  digitalWrite(D2, LOW);
  digitalWrite(D3, LOW);
  digitalWrite(D4, LOW);
}

/**
 * Configure the web server
 */
void handleRoot() {
  Serial.print("Serving web page\n");
  server.send(200, "text/html", WEB_UI);
  Serial.print("Done\n");
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

/**
 * Initialise the device
 */
void setup(void)
{
  // configure serial port
  Serial.begin(115200);
  delay(100);

  config.init(FW_VERSION, CONFIG_FILE);

  Serial.printf("\n\nInitiaising 3mice device\n");
  Serial.printf("Firmware: %s.%d\n", config.deviceType.c_str(), FW_VERSION); 

  // configure the startup state
  config.loadConfig();
  currentPosition = config.loadPosition(currentPosition);
  setPos = currentPosition;

  // start initial connection
  wifiManager.begin();
  // start the update server
  updateManager.begin();

  //Update webpage
  Serial.println("Preparing web interface");

  //Start HTTP server
  Serial.println("Starting HTTP server");
  server.on("/", handleRoot);
  server.onNotFound(handleNotFound);
  server.begin();

  //Start websocket
  Serial.println("Starting websocket server");
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);

  Serial.println("Device initialised\n");
  
  Serial.print("Connect to http://" + config.deviceName + ".local or http://");
  Serial.println(WiFi.localIP());
}

/**
 * main action loop
 */
void loop(void)
{
  /*
     After running setup() the motor might still have
     power on some of the coils. This is making sure that
     power is off the first time loop() has been executed
     to avoid heating the stepper motor draining
     unnecessary current
  */
  if (firstRun) {
    firstRun = false;
    stopPowerToCoils();
  }

  /* check for updates */
  updateManager.update();
  
  //Websocket listner
  webSocket.loop();

  /**
    Serving the webpage
  */
  server.handleClient();

  wifiManager.loop();

  /**
    Manage actions. Steering of the blind
  */
  if (action == "auto") {
    Serial.println("Automatic mode");
    Serial.print("Current position: ");
    Serial.println(currentPosition);
    Serial.print("Path: ");
    Serial.println(path);
    /*
       Automatically open or close blind
    */
    if (currentPosition > path) {
      small_stepper.step(config.blind.reverse ? -1: 1);
      currentPosition = currentPosition - 1;
    } else if (currentPosition < path) {
      small_stepper.step(config.blind.reverse ? 1 : -1);
      currentPosition = currentPosition + 1;
    } else {
      path = 0;
      action = "";
      reportPosition(255);
      Serial.println("Stopped. Reached wanted position");
      savePosition = true;
    }

 } else if (action == "manual" && path != 0) {
    /*
       Manually running the blind
    */
    small_stepper.step(config.blind.reverse ? path : -path);
    currentPosition = currentPosition + path;
  }

  /**
    Storing positioning data and turns off the power to the coils
  */
  if (saveConfig || savePosition) {
    if (saveConfig) {
      config.saveConfig();
      saveConfig = false;
    }
    Serial.print("Storing current position: ");
    Serial.println(currentPosition);
    config.savePosition(currentPosition);
    savePosition = false;

    /*
      If no action is required by the motor make sure to
      turn off all coils to avoid overheating and less energy
      consumption
    */
    stopPowerToCoils();
  }
}
