#ifndef WifiManager_h
#define WifiManager_h

#include <Arduino.h>
#include <DNSServer.h>
#include "ConfigManager.h"

#ifndef WIFI_MANAGER_DEBUG
#define WIFI_MANAGER_DEBUG 2
#endif

/**
 * The 3mice wifi manager.
 * 
 * Provides an API for managing wifi connections. This allows for the user
 * to provide a custom UI skin
 */
class WifiManager {
  public:
    WifiManager(DeviceConfig &config);

    void loop();
    void begin();
    void startAccessPoint();
    void stopAccessPoint();

    void forget();
    void setNewWifi(String newSSID, String newPass);
    void connectNewWifi(String newSSID, String newPass);

    void updateHostname();
    void startDNS();
    
    // debug
    int _debugLevel = WIFI_MANAGER_DEBUG;

  private:
    DeviceConfig *deviceConfig;
    DNSServer *dnsServer;
    bool reconnect = false;
    bool isAccessPoint = false;
    String ssid;
    String pass;

    void serialDebug(int debugLevel, String message);
};

#endif
